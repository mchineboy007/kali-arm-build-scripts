#!/bin/bash 

################################################################################
#  Script            : multibuild.sh
#  Author            : Tyler Hardison <tyler@redhawksecurity.com>
#  Acknowledgements  :
#                    :
#                    :
#  Description       : Builds multiple encrypted images at one time
#  Changelog         : Initial creation
#                    :
#                    : 
#                    :
################################################################################

function usage
{
   echo "usage: multibuild.sh -p <platform> -n <number of images> -m <rns magic server>i -o <outputdir>"
   echo
   echo " -p <platform> (required) one of any of the scripts I.E. rpi2 rpi bbb etc."
   echo " -n <number> (optional) default 1 - number of images to generate"
   echo " -m <hostname> (optional) for RNS specify the magic server"
   echo " -o <outputdir> (optional) specify the output directory for images default is pwd"
}

# parse the arguments

while [ "$1" != "" ]; do
	case $1 in 
	  -p | --platform     ) shift
	                        platform=$1
				;;
	  -n | --numberimages ) shift
	                        images=$1
				;;
	  -m | --magicserver  ) shift
	                        magic=$1
				;;
	  -o | --outputdir    ) shift
	                        outputdir=$1
				;;
	  -? | -h | --help    ) usage
	                        exit
				;;
	  *                   ) usage
	                        exit 1
	esac
	shift
done

if [ "X${platform}" = "X" ]; then
        usage
        exit 1
fi

if [ "X${images}" = "X" ]; then
	images = 1
fi

if [ "X${outputdir}" = "X" ]; then
	outputdir=`pwd`
fi

if [ ! -d "${outputdir}" ]; then
	mkdir -p ${outputdir}
fi

# Start iterating

export PATH=/home/arm-stuff/gcc-arm-linux-gnueabihf-4.7/bin:$PATH

for (( count=0; count < images ; count++ )); do
        bash  ${platform}.sh ${magic}
	id=`cat ${platform}-rolling/this.id | sed 's/\n//g'`
        mkdir -p ${outputdir}/cheat-${id}
	mv ${platform}-rolling/*img* ${outputdir}/cheat-${id}/
	mv ~/rpi${id}* ${outputdir}/cheat-${id}/
        cp ${platform}-rolling/root.pw ${outputdir}/cheat-${id}/
done
