#!/bin/bash

# This is the Raspberry Pi Kali ARM build script - http://www.kali.org/downloads
# A trusted Kali Linux image created by Offensive Security - http://www.offensive-security.com

basedir=`pwd`/rpi-rolling

# Package installations for various sections.
# This will build a minimal XFCE Kali system with the top 10 tools.
# This is the section to edit if you would like to add more packages.
# See http://www.kali.org/new/kali-linux-metapackages/ for meta packages you can
# use. You can also install packages, using just the package name, but keep in
# mind that not all packages work on ARM! If you specify one of those, the
# script will throw an error, but will still continue on, and create an unusable
# image, keep that in mind.

arm="abootimg fake-hwclock ntpdate u-boot-tools"
base="e2fsprogs initramfs-tools kali-defaults kali-menu parted sudo usbutils dropbear cryptsetup busybox jq"
desktop="fonts-croscore fonts-crosextra-caladea fonts-crosextra-carlito gnome-theme-kali gtk3-engines-xfce kali-desktop-xfce kali-root-login lightdm network-manager network-manager-gnome xfce4 xserver-xorg-video-fbdev"
tools="kali-linux winexe"
services="apache2 openssh-server"
extras="iceweasel xfce4-terminal wpasupplicant"

size=14500 # Size of image in megabytes

packages="${arm} ${base} ${desktop} ${tools} ${services} ${extras}"
architecture="armel"
# If you have your own preferred mirrors, set them here.
# After generating the rootfs, we set the sources.list to the default settings.
mirror=http.kali.org
release=rolling

if [ ! -f ${basedir}/bootstrap.done ]
then
	bash ./scripts/build-base-image.sh -a ${architecture} -p ${basedir} -r ${release}
fi

echo 1 > ${basedir}/bootstrap.done

# XXX I don't currently know if this is required for third stage? Or for kernel build??

export MALLOC_CHECK_=0 # workaround for LP: #520465
export LC_ALL=C
export DEBIAN_FRONTEND=noninteractive

cd ${basedir}

if [ ! -f kaliroot.done ] 
then
    bash ../scripts/build-kali-root.sh -a ${architecture} -p "${packages}"
else 
    bash ../scripts/build-kali-root.sh -a ${architecture} -u
fi

echo 1 > kaliroot.done

if [ $? -gt 0 ]
then
  exit 1
fi

rm kali*img 

bash ../scripts/build-kali-diskimage.sh -a ${architecture} -e -m $1 -s ${size} -p ${basedir}

if [ $? -gt 0 ]
then
  echo "Disk image failed to build.. Refusing the continue."
  exit 1
fi

# Kernel section. If you want to use a custom kernel, or configuration, replace
# them in this section.
git clone --depth 1 https://github.com/raspberrypi/linux -b rpi-4.1.y ${basedir}/root/usr/src/kernel
git clone --depth 1 https://github.com/raspberrypi/tools ${basedir}/tools

cd root/usr/src/kernel
git rev-parse HEAD > ../kernel-at-commit
touch .scmversion
export ARCH=arm
export CROSS_COMPILE=${basedir}/tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-

make bcmrpi_defconfig
make -j 3 zImage modules dtbs
make modules_install INSTALL_MOD_PATH=${basedir}/root

cp .config ../rpi-4.0.config

git clone --depth 1 https://github.com/raspberrypi/firmware.git rpi-firmware
cp -rf rpi-firmware/boot/* ${basedir}/bootp/
scripts/mkknlimg arch/arm/boot/zImage ${basedir}/bootp/kernel.img

mkdir -p ${basedir}/bootp/overlays/

cp arch/arm/boot/dts/*.dtb ${basedir}/bootp/
cp arch/arm/boot/dts/overlays/*.dtb* ${basedir}/bootp/overlays/

make mrproper
cp ../rpi-4.0.config .config
make oldconfig modules_prepare

cd ${basedir}

rm -rf root/lib/firmware
cd root/lib
git clone --depth 1 https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git firmware
rm -rf root/lib/firmware/.git

cd ${basedir}

# rpi-wiggle
mkdir -p root/scripts
wget https://raw.github.com/dweeber/rpiwiggle/master/rpi-wiggle -O root/scripts/rpi-wiggle.sh
chmod 755 root/scripts/rpi-wiggle.sh

cp ${basedir}/../misc/zram ${basedir}/root/etc/init.d/zram
chmod +x ${basedir}/root/etc/init.d/zram

cd ${basedir}

# Create the initramfs
mount -t proc proc root/proc
mount -o bind /dev/ root/dev/
mount -o bind /dev/pts root/dev/pts
mount -o bind /sys root/sys
mount -o bind /run root/run

cat << EOF > ${basedir}/root/mkinitram
#!/bin/bash
mkinitramfs -o /boot/initramfs.gz \`ls /lib/modules/ | grep 4 | head -n 1\`
EOF

chmod +x root/mkinitram
LANG=C chroot root /mkinitram

mv ${basedir}/root/boot/initramfs.gz $basedir/bootp/

# Unmount partitions
umount -R ${basedir}/bootp
umount -R ${basedir}/root

cryptsetup luksClose /dev/mapper/crypt_sdcard

kpartx -dv `cat ${basedir}/myloop.dev`
losetup -d `cat ${basedir}/myloop.dev`

rm ${basedir}/myloop.dev

# If you're building an image for yourself, comment all of this out, as you
# don't need the sha1sum or to compress the image, since you will be testing it
# soon.

echo "Generating sha1sum for kali-rolling.img"

sha1sum kali-${architecture}.img > ${basedir}/kali-${architecture}.img.sha1sum
# Don't pixz on 32bit, there isn't enough memory to compress the images.

MACHINE_TYPE=`uname -m`

if [ ${MACHINE_TYPE} == 'x86_64' ]; then
   echo "Compressing kali-${architecture}.img"
   pixz ${basedir}/kali-${architecture}.img ${basedir}/kali-${architecture}.img.xz
   echo "Generating sha1sum for kali-${architecture}.img.xz"
   sha1sum kali-${architecture}.img.xz > ${basedir}/kali-${architecture}.img.xz.sha1sum
fi
