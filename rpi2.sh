#!/bin/bash

# This is the Raspberry Pi2 Kali ARM build script - http://www.kali.org/downloads
# A trusted Kali Linux image created by Offensive Security - http://www.offensive-security.com

basedir=`pwd`/rpi2-rolling

# Package installations for various sections.
# This will build a minimal XFCE Kali system with the top 10 tools.
# This is the section to edit if you would like to add more packages.
# See http://www.kali.org/new/kali-linux-metapackages/ for meta packages you can
# use. You can also install packages, using just the package name, but keep in
# mind that not all packages work on ARM! If you specify one of those, the
# script will throw an error, but will still continue on, and create an unusable
# image, keep that in mind.

arm="abootimg fake-hwclock ntpdate u-boot-tools"
base="e2fsprogs initramfs-tools kali-defaults kali-menu parted sudo usbutils dropbear cryptsetup busybox jq curl elinks cmake daemontools daemontools-run svtools libnss-ldapd libpam-ldapd libpam-google-authenticator rsyslog socat"
desktop="fonts-croscore fonts-crosextra-caladea fonts-crosextra-carlito gnome-theme-kali gtk3-engines-xfce kali-desktop-xfce kali-root-login lightdm network-manager network-manager-gnome xfce4 xserver-xorg-video-fbdev htop kali-desktop-common kali-menu cinnamon-desktop-environment fonts-hack-otf fonts-hack-ttf fonts-hack-web"
tools="kali-linux-full winexe sysstat armitage gnome-tweak-tool ruby snort snort-rules-default snort-common oinkmaster snort-common-libraries"
services="apache2 openssh-server "
extras="iceweasel xfce4-terminal wpasupplicant"

#size=15279 # Size of image in megabytes
size=29988 # 32gb Cana Card

packages="${arm} ${base} ${desktop} ${tools} ${services} ${extras}"
architecture="armhf"
# If you have your own preferred mirrors, set them here.
# After generating the rootfs, we set the sources.list to the default settings.
mirror=http.kali.org
release=rolling

if [ ! -f ${basedir}/bootstrap.done ]
then
        echo "Bootstrapping base image..."
	bash ./scripts/build-base-image.sh -a ${architecture} -p ${basedir} -r ${release} -n rnskali > bootstrap.log 2>&1
        echo "Bootstrap complete..."
fi

echo 1 > ${basedir}/bootstrap.done

# XXX I don't currently know if this is required for third stage? Or for kernel build??

export MALLOC_CHECK_=0 # workaround for LP: #520465
export LC_ALL=C
export DEBIAN_FRONTEND=noninteractive

cd ${basedir}

if [ ! -f kaliroot.done ] 
then
    echo "Building Kali Root..."
    bash ../scripts/build-kali-root.sh -a ${architecture} -p "${packages}" > rootbuild.log 2>&1
else 
    echo "Updating Kali Root..."
    bash ../scripts/build-kali-root.sh -a ${architecture} -u > rootbuild.log 2>&1
fi

if [ $? -gt 0 ]
then
  echo "Something went wrong while building the root.. check `pwd`/rootbuild.log for details.."
  exit 1
fi

echo "Kali Root Complete..."

echo 1 > kaliroot.done

echo "Removing old image.."

rm kali*img 

if [ $? -gt 0 ] 
then
  echo "Unable to remove the image.. Please check that the image file is not in use.."
  exit 1;
fi

echo "Building new disk image.."

bash ../scripts/build-kali-diskimage.sh -a ${architecture} -e -m $1 -s ${size} -p ${basedir} > diskimage.log 2>&1

if [ $? -gt 0 ]
then
  echo "Disk image failed to build.. Refusing the continue."
  exit 1
fi

# Kernel section. If you want to use a custom kernel, or configuration, replace
# them in this section.

echo "Cloning kernel source into the new image..."

git clone --depth 1 -b rpi-4.4.y https://github.com/raspberrypi/linux ${basedir}/root/usr/src/kernel > kernelclone.log 2>&1

echo "Cloning RPi tools into new image..."

git clone --depth 1 https://github.com/raspberrypi/tools ${basedir}/tools > toolsclone.log 2>&1

cd root/usr/src/kernel

git rev-parse HEAD > ../kernel-at-commit
touch .scmversion
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-
export KERNEL=kernel7

echo "Building kernel image.."
yes | make bcm2709_defconfig > kernconfig.log 2>&1
perl -pi -e 's/# (.*?CRYPT.*?)\s+is not.*$/$1=y/g' .config 
perl -pi -e 's/# (.*?AES.*?)\s+is not.*$/$1=y/g' .config
perl -pi -e 's/CONFIG_CRYPTO_XTS=m/CONFIG_CRYPTO_XTS=y/g' .config
make olddefconfig >> kernconfig.log 2>&1
make -j `grep -i proc /proc/cpuinfo | wc -l` zImage modules dtbs >> kernconfig.log 2>&1
make modules_install INSTALL_MOD_PATH=${basedir}/root >> kernconfig.log 2>&1

cp .config ../rpi2-4.0.config

echo "Cloning firmware for RPi..."

git clone --depth 1 https://github.com/raspberrypi/firmware.git rpi-firmware >> firmware.log 2>&1
cp -rf rpi-firmware/boot/* ${basedir}/bootp/

echo "Creating kernel image file..."
scripts/mkknlimg arch/arm/boot/zImage ${basedir}/bootp/kernel7.img >> kernconfig.log 2>&1

mkdir -p ${basedir}/bootp/overlays/

cp arch/arm/boot/dts/*.dtb ${basedir}/bootp/
cp arch/arm/boot/dts/overlays/*.dtb* ${basedir}/bootp/overlays/

echo "Cleaning kernel source..."
make mrproper >> kernconfig.log 2>&1
cp ../rpi2-4.0.config .config
make olddefconfig modules_prepare >> kernconfig.log 2>&1

cd ${basedir}

rm -rf root/lib/firmware
cd root/lib

echo "Cloning Linux firmware from kernel.org..."
git clone --depth 1 https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git firmware >> kernconfig.log 2>&1
rm -rf root/lib/firmware/.git

cd ${basedir}

# rpi-wiggle

echo "Installing wiggle..."
mkdir -p root/scripts
wget https://raw.github.com/dweeber/rpiwiggle/master/rpi-wiggle -O root/scripts/rpi-wiggle.sh > wiggle.log 2>&1
chmod 755 root/scripts/rpi-wiggle.sh

cp ${basedir}/../misc/zram ${basedir}/root/etc/init.d/zram
chmod +x ${basedir}/root/etc/init.d/zram

echo "Cloning RPi userland software..."
cd ${basedir}/root/usr/src
git clone https://github.com/raspberrypi/userland.git >> userland.log 2>&1

echo "Building userland software..."
cd userland
mkdir -p build/arm-linux/release
pushd build/arm-linux/release
cmake -DCMAKE_TOOLCHAIN_FILE=../../../makefiles/cmake/toolchains/arm-linux-gnueabihf.cmake -DCMAKE_BUILD_TYPE=Release ../../.. > userland.log 2>&1
make -j4 >> userland.log 2>&1
make install DESTDIR=${basedir}/root/opt/vc >> userland.log 2>&1
popd
cd ${basedir}

echo "Create initramfs..."

# Create the initramfs
mount -t proc proc root/proc
mount -o bind /dev/ root/dev/
mount -o bind /dev/pts root/dev/pts
mount -o bind /sys root/sys
mount -o bind /run root/run

cat << EOF > ${basedir}/root/mkinitram
#!/bin/bash
ln -sf /usr/share/zoneinfo/US/Pacific-New /etc/localtime
apt-get clean
rm /usr/share/initramfs-tools/hooks/klibc^i-t
mkinitramfs -v -k -o /boot/initramfs.gz \`ls /lib/modules/ | grep 4 | head -n 1\`
rm /root/.curlpacket
rm /root/.bash_history
rm /0
rm /cleanup
rm /mkinitram
EOF

chmod +x root/mkinitram
LANG=C chroot root /mkinitram >> mkinitram.log 2>&1

mv ${basedir}/root/boot/initramfs.gz $basedir/bootp/

echo "Cleanup..."
# Unmount partitions
umount -lfR ${basedir}/bootp
umount -lfR ${basedir}/root

cryptsetup luksClose /dev/mapper/crypt_sdcard

kpartx -dv `cat ${basedir}/myloop.dev` >> cleanup.log 2>&1
losetup -d `cat ${basedir}/myloop.dev` >> cleanup.log 2>&1

rm ${basedir}/myloop.dev

# If you're building an image for yourself, comment all of this out, as you
# don't need the sha1sum or to compress the image, since you will be testing it
# soon.

echo "Generating sha1sum for kali-rolling.img"

sha1sum kali-${architecture}.img > ${basedir}/kali-${architecture}.img.sha1sum
