#!/bin/bash

#######################################################################
## Script          : build-kali-root.sh
## Author          : Tyler Hardison <tyler@seraph-net.net>
## Acknowledgments : Offensive Security is the original author. I'm
##                 : just taking their original work and making it 
##                 : more modular.
## Changelog       : <2016.2.9-TH> Creation of original script.
##                 :
##                 :
##                 :
##                 :
## Description     : Takes a bootstrapped fs and sprinkles kali magic
#######################################################################

function usage 
{
	echo "usage: build-kali-root.sh -a architecture"
	echo 
	echo "-a architecture (required) armel,armhf,..."
	echo "-p 'list of packages' (required)"
	echo "-u update only, ignores packages"
}

# parse arguments

while [ "$1" != "" ]; do
   case $1 in 
     -a | --architecture ) shift
	                       architecture=$1
						   ;;
	 -p | --packages     ) shift
	                       packages=$1
						   ;;
     -u | --update-only  ) updateonly=1
                           ;;
	 -? | -h | --help    ) usage
	                       exit
						   ;;
	 * )                   usage
	                       exit 1
   esac
   shift
done   

if [ "X${architecture}" = "X" ]
then
  usage
  exit 1
fi  

mount -t proc proc kali-${architecture}/proc
mount -o bind /proc/sys/fs/binfmt_misc kali-${architecture}/proc/sys/fs/binfmt_misc
mount -o bind /dev/ kali-${architecture}/dev/
mount -o bind /dev/pts kali-${architecture}/dev/pts
mount -o bind /sys kali-${architecture}/sys
mount -o bind /run kali-${architecture}/run

if [ "X${updateonly}" = "X" ]
then
cat << EOF > kali-${architecture}/etc/apt/sources.list
deb http://http.kali.org/kali kali-rolling main non-free contrib
deb-src http://http.kali.org/kali kali-rolling main non-free contrib
EOF

cat << EOF > kali-${architecture}/debconf.set
console-common console-data/keymap/policy select Select keymap from full list
console-common console-data/keymap/full select en-latin1-nodeadkeys
EOF

# Create a random root password.

openssl rand -base64 18 | sed 's/\n//' > root.pw 
rootpw=`cat root.pw`

cat << EOF > kali-${architecture}/third-stage
#!/bin/bash
dpkg-divert --add --local --divert /usr/sbin/invoke-rc.d.chroot --rename /usr/sbin/invoke-rc.d
cp /bin/true /usr/sbin/invoke-rc.d
echo -e "#!/bin/sh\nexit 101" > /usr/sbin/policy-rc.d
chmod +x /usr/sbin/policy-rc.d

apt-get update
apt-get --yes --allow-downgrades --allow-remove-essential --allow-change-held-packages install locales-all

debconf-set-selections /debconf.set
rm -f /debconf.set
apt-get update
apt-get -y install git-core binutils ca-certificates initramfs-tools u-boot-tools
apt-get -y install locales console-common less nano git
echo "root:${rootpw}" | chpasswd
sed -i -e 's/KERNEL\!=\"eth\*|/KERNEL\!=\"/' /lib/udev/rules.d/75-persistent-net-generator.rules
rm -f /etc/udev/rules.d/70-persistent-net.rules
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --allow-downgrades --allow-remove-essential --allow-change-held-packages install $packages
apt-get --yes --allow-downgrades --allow-remove-essential --allow-change-held-packages dist-upgrade
apt-get --yes --allow-downgrades --allow-remove-essential --allow-change-held-packages autoremove

# Because copying in authorized_keys is hard for people to do, let's make the
# image insecure and enable root login with a password.

sed -i -e 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

grep 'GatewayPorts yes' /etc/ssh/sshd_config || echo "GatewayPorts yes" >> /etc/ssh/sshd_config

sed -i -e 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/' /etc/ssh/sshd_config

echo "%Engineers ALL=(ALL:ALL) ALL" > /etc/sudoers.d/engineers

sed -i -e 's/# output alert_syslog/output alert_syslog/' /etc/snort/snort.conf

update-rc.d ssh enable
update-rc.d nslcd enable
update-rc.d rsyslog enable

rm -f /usr/sbin/policy-rc.d
rm -f /usr/sbin/invoke-rc.d
dpkg-divert --remove --rename /usr/sbin/invoke-rc.d

rm -f /third-stage
EOF

chmod +x kali-${architecture}/third-stage
LANG=C chroot kali-${architecture} /third-stage

cat << EOF > kali-${architecture}/cleanup
#!/bin/bash
rm -rf /root/.bash_history
apt-get update
apt-get clean
ln -sf /run/NetworkManager/resolv.conf /etc/resolv.conf
EOF

# Enable login over serial
echo "T0:23:respawn:/sbin/agetty -L ttyAMA0 115200 vt220" >> kali-${architecture}/etc/inittab

chmod +x kali-${architecture}/cleanup
LANG=C chroot kali-${architecture} /cleanup

else 

cat << EOF > kali-${architecture}/update
#!/bin/bash
apt-get update
apt-get --yes --allow-downgrades --allow-remove-essential --allow-change-held-packages dist-upgrade

rm /update
EOF
chmod +x kali-${architecture}/update
LANG=C chroot kali-${architecture} /update 

fi

# Do some perl things for RNS

cp -Rp ../../rns_cheat/RNS-CheatClient kali-${architecture}/
cp -Rp ../../rns_cheat/cheatscripts kali-${architecture}/tmp/

cat << EOF > kali-${architecture}/doperl
#!/bin/bash
curl -L https://cpanmin.us | perl - App::cpanminus
cpanm --verbose --notest LWP YAML::Syck JSON DBD::SQLite
cd /RNS-CheatClient
perl Makefile.PL
make && make install
cd /

mkdir -p /service/tunnel-ldaps/log
mkdir -p /service/tunnel-ssh/log
mkdir -p /service/tunnel-syslog/log
mkdir -p /service/fluentd/log
mkdir -p /service/socat-pipe/log

mv /tmp/cheatscripts/ldaps-run /service/tunnel-ldaps/run
mv /tmp/cheatscripts/ssh-run /service/tunnel-ssh/run
mv /tmp/cheatscripts/ldaps-log /service/tunnel-ldaps/log/run
mv /tmp/cheatscripts/ssh-log /service/tunnel-ssh/log/run
mv /tmp/cheatscripts/stash-run /service/tunnel-syslog/run
mv /tmp/cheatscripts/stash-log /service/tunnel-syslog/log/run
mv /tmp/cheatscripts/fluentd-run /service/fluentd/run
mv /tmp/cheatscripts/fluentd-log /service/fluentd/log/run
mv /tmp/cheatscripts/socatpipe-run /service/socat-pipe/run
mv /tmp/cheatscripts/socatpipe-log /service/socat-pipe/log/run


mv /tmp/cheatscripts/nslcd.conf /etc/
mv /tmp/cheatscripts/syncusers.pl /etc/cron.hourly/
mv /tmp/cheatscripts/known_hosts /root/.ssh/

chmod 600 /root/.ssh/known_hosts

chmod +x /etc/cron.hourly/syncusers.pl
chmod +x /service/tunnel-ldaps/run
chmod +x /service/tunnel-ldaps/log/run
chmod +x /service/tunnel-ssh/run
chmod +x /service/tunnel-ssh/log/run
chmod +x /service/tunnel-syslog/run
chmod +x /service/tunnel-syslog/log/run
chmod +x /service/fluentd/run
chmod +x /service/fluentd/log/run
chmod +x /service/socat-pipe/run
chmod +x /service/socat-pipe/log/run

grep ldap /etc/nsswitch.conf || sed -i -e 's/compat/compat ldap/g' /etc/nsswitch.conf

ln -sf /service/tunnel-ldaps /etc/service/
ln -sf /service/tunnel-ssh /etc/service/
ln -sf /service/tunnel-syslog /etc/service/
ln -sf /service/fluentd /etc/service/
ln -sf /service/socat-pipe /etc/service/

apt-get -y remove nscd
apt-get -y autoremove
apt-get -y install traceroute iputils-tracepath mtr

grep 'pam_google_auth' /etc/pam.d/common-auth || echo "auth required pam_google_authenticator.so" >> /etc/pam.d/common-auth
grep '42185' /etc/rsyslog.conf || echo "*.* @127.0.0.1:42185" >> /etc/rsyslog.conf

gem install fluentd --no-ri --no-rdoc
gem install fluent-plugin-snmptrap

fluentd --setup /opt/fluentd

mv /tmp/cheatscripts/td-agent.conf /opt/fluentd/fluentd.conf

rm -rf /RNS-CheatClient
rm /usr/share/initramfs-tools/hooks/klibc^i-t # ignored: not alphanumeric or '_' file

rm /doperl
EOF

chmod +x kali-${architecture}/doperl
LANG=C chroot kali-${architecture} /doperl

umount kali-${architecture}/proc/sys/fs/binfmt_misc
umount kali-${architecture}/dev/pts
umount kali-${architecture}/dev/
umount kali-${architecture}/proc
umount kali-${architecture}/run
umount kali-${architecture}/sys
