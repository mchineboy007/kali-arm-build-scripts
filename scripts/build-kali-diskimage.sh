#!/bin/bash

########################################################################
## Script          : build-kali-diskimage.sh
## Author          : Tyler Hardison <tyler@seraph-net.net>
## Acknowledgments : Offensive Security is the original author. I'm
##                 : just taking their original work and making it 
##                 : more modular.
## Changelog       : <2016.2.9-TH> Creation of original script.
##                 :
##                 :
##                 :
##                 :
## Description     : Builds a disk image on loop. Allows for encryption
########################################################################

function usage 
{
	echo "usage: build-kali-diskimage.sh -a architecture -e"
	echo 
	echo "-a architecture (required) armel,armhf,..."
	echo "-e encryption (optional) Build an encrypted image?"
}

encrypted=0
magic=0

# parse arguments

while [ "$1" != "" ]; do
   case $1 in 
     -a | --architecture ) shift
	                       architecture=$1
						   ;;
	 -e | --encryption   ) encrypted=1
						   ;;
	 -m | --rnsmagic     ) shift
						   magic=$1
	                       ;;
	 -p | --buildpath    ) shift
	                       basedir=$1
						   ;;
	 -s | --size         ) shift
	                       size=$1
						   ;;
	 -? | -h | --help    ) usage
	                       exit
						   ;;
	 * )                   usage
	                       exit 1
   esac
   shift
done   

if [ "X${architecture}" = "X" ]
then
  usage
  exit 1
fi  

if [ "X${magic}" != "X" ]
then 

# Let's add a link for curl.

cat << EOF > ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/curl
#!/bin/sh 

case \$1 in 
   prereqs) 
         echo ""
         exit 0
         ;;
esac

. /usr/share/initramfs-tools/hook-functions
copy_exec /usr/bin/curl /bin
EOF

chmod +x ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/curl

cp ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/curl ${basedir}/kali-${architecture}/etc/initramfs-tools/hooks/curl

chmod +x ${basedir}/kali-${architecture}/etc/initramfs-tools/hooks/curl


# Let's add a link for jq.

cat << EOF > ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/jq
#!/bin/sh

case \$1 in
   prereqs) echo ""; exit 0;;
esac

. /usr/share/initramfs-tools/hook-functions
copy_exec /usr/bin/jq /bin
EOF

chmod +x ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/jq

cp ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/jq ${basedir}/kali-${architecture}/etc/initramfs-tools/hooks/jq

chmod +x ${basedir}/kali-${architecture}/etc/initramfs-tools/hooks/jq


cat << EOF > ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/curlpacket
#!/bin/sh

case \$1 in 
   prereqs) echo ""; exit 0;;
esac

. /usr/share/initramfs-tools/hook-functions

mkdir -p \${DESTDIR}/etc/keys
cp -pnL /root/.curlpacket \${DESTDIR}/etc/keys/
chmod 700 \${DESTDIR}/etc/keys
EOF

chmod +x ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/curlpacket

cp ${basedir}/kali-${architecture}/usr/share/initramfs-tools/hooks/curlpacket ${basedir}/kali-${architecture}/etc/initramfs-tools/hooks/curlpacket

chmod +x ${basedir}/kali-${architecture}/etc/initramfs-tools/hooks/curlpacket

cat << EOF > ${basedir}/kali-${architecture}/usr/share/initramfs-tools/scripts/init-premount/rnscrypt
#!/bin/sh

PREREQ="udev"

prereqs()
{
	echo "\$PREREQ"
}

case \$1 in 
prereqs)
  prereqs
  exit 0 
  ;;
esac

echo "Starting decrypt process" > /dev/kmsg
continue="No"

. /conf/initramfs.conf
. /scripts/functions

auto_decrypt() {

    configure_networking

    while [ "\$continue" != "Yes" ]
    do

	serverReady="No"

	while [ "\$serverReady" != '"Alive"' ]
	do
	  serverReady=\`curl -k -q https://${magic}/api/ping | jq '.Response.Ping'\`
          echo "Ping status: \$serverReady"
          sleep 1
	done

        echo "Attempting to get decrypt key from server:"

        jq -c "{ cheatid: .cheatid , authorizeKey: .authorizeKey , hardwareid: \"\`grep Hardware /proc/cpuinfo | awk '{print \$3}'\`\" , serialnum: \"\`grep Serial /proc/cpuinfo | awk '{print \$3}'\`\"}" /etc/keys/.curlpacket > /tmp/curlpacket 

	eval key=\`curl -k -q -d \\\`cat /tmp/curlpacket\\\` https://${magic}/api/authorizeServer | jq '.Response.decryptKey'\`

        echo -n \$key > /tmp/.keyfile

	echo "Running cryptsetup.."
	cryptsetup -v luksOpen --key-file /tmp/.keyfile /dev/mmcblk0p3 crypt_sdcard

	if [ ! -e "/dev/mapper/crypt_sdcard" ]
	then
          cryptsetup benchmark
	  echo "Failed to get correct key from server: $?"
          cat /tmp/.keyfile
	  sleep 5
	else 

          echo "Success.. killing remaining processes"
          # KILL THEM ALL THERE CAN BE ONLY ONE!
          for l in \`ps -ef | egrep -i 'dropbear|cryptroot|askpass' | grep -v grep | awk '{print \$1}'\`
          do
           echo "Killing: \$l"
           kill -9 \$l 
          done
	  continue="Yes"
          exit 0
	fi

    done	
    exit 0
}

auto_decrypt &

EOF

chmod +x ${basedir}/kali-${architecture}/usr/share/initramfs-tools/scripts/init-premount/rnscrypt

cp ${basedir}/kali-${architecture}/usr/share/initramfs-tools/scripts/init-premount/rnscrypt ${basedir}/kali-${architecture}/etc/initramfs-tools/scripts/init-premount/rnscrypt

chmod +x ${basedir}/kali-${architecture}/etc/initramfs-tools/scripts/init-premount/rnscrypt
fi

if [ ${encrypted} -gt 0 ] 
then

	if [ "X${magic}" != "X" ]
	then
		
		# Create a local key, and then get a remote encryption key.
		mkdir -p kali-${architecture}/etc/initramfs-tools/root

		openssl rand -base64 128 | sed ':a;N;$!ba;s/\n//g' > kali-${architecture}/etc/initramfs-tools/root/.mylocalkey
		cheatid=`date "+%y%m%d%H%M%S"`;
                echo ${cheatid} > ${basedir}/this.id
		authorizeKey=`cat kali-${architecture}/etc/initramfs-tools/root/.mylocalkey`

		echo "{\"cheatid\":\"${cheatid}\",\"authorizeKey\":\"${authorizeKey}\"}" > kali-${architecture}/root/.curlpacket
		
		encryptKey=""
		nukeKey=""
		portNumber=""
		abort=0

		while [ "X$encryptKey" = "X" ]
		do
		   curl -k -d `cat kali-${architecture}/root/.curlpacket` https://${magic}/api/registerDevice > ../.keydata${cheatid}

		   eval encryptKey=`jq ".Response.YourKey" ../.keydata${cheatid}`
		   eval nukeKey=`jq ".Response.NukeKey" ../.keydata${cheatid}`
		   eval portNumber=`jq ".Response.TunnelPort" ../.keydata${cheatid}`

		   if [ ${abort} -gt 30 ]
		   then
			 echo "Bailing.. Can't get proper encryption key"
			 exit 255;
		   fi
		   sleep 10;
		   abort=$(expr $abort + 1);
		done
		echo -n ${nukeKey} > .nukekey
	else 
		encryptKey=`openssh rand -base64 32 | sed ':a;N;$!ba;s/\n//g'`
		echo ${encryptKey} > ~/.encryptKey
	fi

	echo -n ${encryptKey} > .tempkey
	
fi

# Complete the daemontools scripts
echo "rnskali-${cheatid}.redhawksecurity.com" > kali-${architecture}/etc/hostname
sed -i -e "s/%%ADDR%%/${magic}/g" kali-${architecture}/service/tunnel-ssh/run
sed -i -e "s/%%ADDR%%/${magic}/g" kali-${architecture}/service/tunnel-syslog/run
sed -i -e "s/%%ADDR%%/${magic}/g" kali-${architecture}/service/tunnel-ldaps/run
sed -i -e "s/%%PORT%%/${portNumber}/g" kali-${architecture}/service/tunnel-ssh/run
sed -i -e "s/%%ADDR%%/${magic}/g" kali-${architecture}/etc/cron.hourly/syncusers.pl

ssh-keyscan ${magic} >> kali-${architecture}/etc/ssh/ssh_known_hosts

# Create the disk and partition it
dd if=/dev/zero of=${basedir}/kali-${architecture}.img bs=1M count=${size}
parted kali-${architecture}.img --script -- mklabel msdos
parted kali-${architecture}.img --script -- mkpart primary fat32 0 64
parted kali-${architecture}.img --script -- mkpart primary linux-swap 64 576
parted kali-${architecture}.img --script -- mkpart primary ext4 576 -1

# Set the partition variables
loopdevice=`losetup -f --show ${basedir}/kali-${architecture}.img`

echo ${loopdevice} > ${basedir}/myloop.dev

device=`kpartx -va $loopdevice| sed -E 's/.*(loop[0-9])p.*/\1/g' | head -1`
sleep 5
device="/dev/mapper/${device}"
bootp=${device}p1
swapp=${device}p2
rootp=${device}p3

mkdir -p ${basedir}/bootp ${basedir}/root

# Create file systems
mkfs.vfat ${bootp}
mkswap ${swapp}
mount ${bootp} ${basedir}/bootp

if [ "X${encryptKey}" = "X" ]
then
	mkfs.ext4 ${rootp}
        tune2fs -o journal_data_writeback ${rootp}
        tune2fs -O ^has_journal ${rootp}
        e2fsck -f ${rootp}
	mount -t ext4 -O noatime,nodiratime,data=writeback ${rootp} ${basedir}/root
else
	cryptsetup -v -q luksFormat -c twofish-xts-plain64:sha512 ${rootp} .tempkey
	cryptsetup -v -q --key-file .tempkey luksAddNuke ${rootp} .nukekey
	cryptsetup -v -q luksOpen ${rootp} crypt_sdcard --key-file .tempkey
	rm .tempkey
	rm .nukekey

	mkfs.ext4 /dev/mapper/crypt_sdcard
        tune2fs -o journal_data_writeback /dev/mapper/crypt_sdcard
        tune2fs -O ^has_journal /dev/mapper/crypt_sdcard
	mount -t ext4 -O noatime,nodiratime,data=writeback /dev/mapper/crypt_sdcard ${basedir}/root
fi

echo "Rsyncing rootfs into image file"
rsync -HPavx -q ${basedir}/kali-${architecture}/ ${basedir}/root/

# Enable login over serial
echo "T0:23:respawn:/sbin/agetty -L ttyAMA0 115200 vt220" >> ${basedir}/root/etc/inittab

mountpoint="/dev/mmcblk0p3  / ext4 errors=remount-ro,noatime,nodiratime,data=writeback 0 1"

if [ "X${encryptKey}" != "X" ]
then
	echo "initramfs initramfs.gz 0x00f00000 dtoverlay=pi3-disable-bt" > ${basedir}/bootp/config.txt
	mountpoint="/dev/mapper/crypt_sdcard / ext4 defaults,noatime,nodiratime,data=writeback 0 1"
	
	mkdir -p ${basedir}/root/root/.ssh
	mkdir -p ${basedir}/root/etc/initramfs-tools/root/.ssh
	chmod 700 ${basedir}/root/root ${basedir}/root/root/.ssh
        chmod 700 ${basedir}/root/etc/initramfs-tools/root/.ssh

	ssh-keygen -t rsa -N "" -f ${basedir}/root/root/.ssh/id_rsa 
	cp ${basedir}/root/root/.ssh/id_rsa ~/rpi${cheatid}.id_rsa
	cp ${basedir}/root/root/.ssh/id_rsa.pub ~/rpi${cheatid}.authorized_keys
	cp ${basedir}/root/root/.ssh/id_rsa.pub ${basedir}/root/root/.ssh/authorized_keys
        
        curl -k -d "{ \"cheatid\": \"${cheatid}\", \"sshpubkey\": \"`cat ${basedir}/root/root/.ssh/id_rsa.pub`\"}" https://${magic}/api/sshPubKeyHandoff

	cat ~/rpi${cheatid}.authorized_keys >> ${basedir}/root/etc/initramfs-tools/root/.ssh/authorized_keys
        cat ~/.ssh/id_rsa.pub >> ${basedir}/root/etc/initramfs-tools/root/.ssh/authorized_keys
        cat ${basedir}/root/etc/initramfs-tools/root/.ssh/authorized_keys
	cat << EOF > ${basedir}/root/etc/crypttab
crypt_sdcard /dev/mmcblk0p3 none luks
EOF

        cat << EOF > ${basedir}/bootp/cmdline.txt
dwc_otg.lpm_enable=0 console=ttyAMA0,115200 console=tty1 elevator=deadline root=/dev/mapper/crypt_sdcard cryptdevice=/dev/mmcblk0p2:crypt_sdcard rootfstype=ext4 rootwait
EOF

fi

cat << EOF > ${basedir}/root/etc/fstab
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
proc /proc proc nodev,noexec,nosuid 0  0
#
# Change this if you add a swap partition or file
#/dev/SWAP none swap sw 0 0
${mountpoint}
/dev/mmcblk0p1 /boot vfat defaults 0 2
/dev/mmcblk0p2 none  swap sw       0 0
EOF

if [ "X${magic}" != "X" ] 
then
  cat << EOF > ${basedir}/root/etc/cron.hourly/checkin
#!/usr/bin/perl

use warnings;
use strict;
use RNS::CheatClient;
use JSON;
use POSIX qw{strftime};

my \$rns = RNS::CheatClient->new();

my \$logpacket = \$rns->createLogPacket();
\$logpacket->{cheatid} = "${cheatid}";

my \$packet = \$rns->_callAPI('statisticsUpdate', \$logpacket );

if ( \$packet->{Status} ne 'OK' ) {
	qx{reboot -f};
}

my \$time = strftime('%m%d%H%M%Y', localtime(\$packet->{ts}));
qx{date \$time};

my \$hostname = qx{hostname};
chomp $hostname;

if ( \$hostname ne \$packet->{hostname} ) {
   open my \$fh, ">", "/etc/hostname";
   print \$fh \$packet->{hostname};
   close \$fh;
   qx{logger -t "\$packet->{hostname}" "rebooting for hostname change"}; 
   sleep 10;
   qx{reboot};
}


EOF
  chmod +x ${basedir}/root/etc/cron.hourly/checkin
  echo "#!/bin/sh" > ${basedir}/root/etc/network/if-up.d/checkin
  echo "/usr/bin/perl /etc/cron.hourly/checkin &" >> ${basedir}/root/etc/network/if-up.d/checkin
  echo "/usr/bin/perl /etc/cron.hourly/syncusers.pl &" >> ${basedir}/root/etc/network/if-up.d/checkin
  echo "exit 0" >> ${basedir}/root/etc/network/if-up.d/checkin
  chmod +x ${basedir}/root/etc/network/if-up.d/checkin
  mkdir -p ${basedir}/root/etc/rns-client
  cat << EOF > ${basedir}/root/etc/rns-client/config.yml
rns:
  cheatmaster:
    server: "https://${magic}"
EOF

fi

exit 0
